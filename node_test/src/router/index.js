import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import Test from '../components/Test.vue'
import Test2 from '../components/Test2.vue'

const router = new VueRouter({
    routes: [
        { path: '/', component: Test },
        { path: '/test2', component: Test2 },
        { path: '*', redirect: '/' }
    ]
})
export default router