import Vue from 'vue'
import App from './App.vue'
import Test from './components/Test.vue'
import store from './store'
import router from './router'

Vue.component('Test1', Test)
new Vue({
    el: '#app',
    render: h => h(App),
    store: store,
    router: router
})