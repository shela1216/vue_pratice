import Vue from 'vue'
import Meta from 'vue-meta'
import Router from 'vue-router'

Vue.use(Router)
Vue.use(Meta)

export function createRouter(store) {
  return new Router({
    mode: 'history',
    scrollBehavior: (to, from, savedPosition) => ({ x: 0, y: 0 }),
    routes: [{
      path: '/',
      component: () => require.ensure([], () => require('pages/Intro').default)
        // 無法通過檢查component: () => import('pages/Intro')
    }, {
      path: '/about',
      component: require('pages/About').default
    }, {
      path: '/new',
      component: () => require.ensure([], () => require('pages/New').default)
    }, {
      path: '*',
      redirect: '/'
    }]
  })
}
